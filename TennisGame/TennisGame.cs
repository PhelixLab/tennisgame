﻿using System;
using System.Collections.Generic;

namespace Tennis
{
    public class TennisGame
    {
        private int _firstPlayerScore;

        private readonly Dictionary<int, string> _scoreLookUp = new Dictionary<int, string>
        {
            {0,"Love" },
            {1,"Fifteen" },
            {2,"Thirty" },
            {3,"Forty" },
        };

        private int _secondPlayerScore;

        public string GetScore()
        {
            if (IsSameScore())
            {
                return IsDeuce() ? Deuce() : SameScore();
            }

            if (IsGamePoint())
            {
                return IsWin() ? Win() : Adv();

            }

            return LookUpScore();
        }

        private string LookUpScore()
        {
            return _scoreLookUp[_firstPlayerScore] + "_" + _scoreLookUp[_secondPlayerScore];
        }

        private string SameScore()
        {
            return _scoreLookUp[_firstPlayerScore] + "_All";
        }

        private static string Deuce()
        {
            return "Deuce";
        }

        private bool IsWin()
        {
            return Math.Abs(_firstPlayerScore - _secondPlayerScore) > 1;
        }

        private string Adv()
        {
            return GetAdvPlayer() + "_Adv";
        }

        private string Win()
        {
            return GetAdvPlayer() + "_Win";
        }

        private string GetAdvPlayer()
        {
            return _firstPlayerScore > _secondPlayerScore ? "FirstPlayer" : "SecondPlayer";
        }

        private bool IsGamePoint()
        {
            return _firstPlayerScore > 3 || _secondPlayerScore > 3;
        }

        private bool IsDeuce()
        {
            return _firstPlayerScore >= 3;
        }

        private bool IsSameScore()
        {
            return _firstPlayerScore == _secondPlayerScore;
        }

        public void FirstPlayerGotScore()
        {
            _firstPlayerScore++;
        }

        public void SecondPlayerGotScore()
        {
            _secondPlayerScore++;
        }
    }
}