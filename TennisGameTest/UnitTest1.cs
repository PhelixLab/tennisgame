﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tennis;

namespace TennisGameTest
{
    [TestClass]
    public class UnitTest1
    {
        private TennisGame _tennisGame = new TennisGame();

        [TestMethod]
        public void Love_All()
        {
            ScoreShouldBe("Love_All");
        }

        [TestMethod]
        public void Fifteen_Love()
        {
            FirstPlayerGotScore(1);
            ScoreShouldBe("Fifteen_Love");
        }

        [TestMethod]
        public void Thirty_Love()
        {
            FirstPlayerGotScore(2);
            ScoreShouldBe("Thirty_Love");
        }

        [TestMethod]
        public void Forty_Love()
        {
            FirstPlayerGotScore(3);
            ScoreShouldBe("Forty_Love");
        }

        [TestMethod]
        public void Love_Fifteen()
        {
            SecondPlayerGotScore(1);
            ScoreShouldBe("Love_Fifteen");
        }

        [TestMethod]
        public void Fifteen_All()
        {
            FirstPlayerGotScore(1);
            SecondPlayerGotScore(1);
            ScoreShouldBe("Fifteen_All");
        }

        [TestMethod]
        public void Deuce_3v3()
        {
            FirstPlayerGotScore(3);
            SecondPlayerGotScore(3);
            ScoreShouldBe("Deuce");
        }

        [TestMethod]
        public void Deuce_4v4()
        {
            FirstPlayerGotScore(4);
            SecondPlayerGotScore(4);
            ScoreShouldBe("Deuce");
        }

        [TestMethod]
        public void FirstPlayerAdv()
        {
            FirstPlayerGotScore(5);
            SecondPlayerGotScore(4);
            ScoreShouldBe("FirstPlayer_Adv");
        }
        [TestMethod]
        public void SecondPlayerAdv()
        {
            FirstPlayerGotScore(3);
            SecondPlayerGotScore(4);
            ScoreShouldBe("SecondPlayer_Adv");
        }
        [TestMethod]
        public void SecondPlayerWin_3v5()
        {
            FirstPlayerGotScore(3);
            SecondPlayerGotScore(5);
            ScoreShouldBe("SecondPlayer_Win");
        }
        [TestMethod]
        public void FirstPlayerWin_4v2()
        {
            FirstPlayerGotScore(4);
            SecondPlayerGotScore(2);
            ScoreShouldBe("FirstPlayer_Win");
        }
        [TestMethod]
        public void Fifteen_Thirty()
        {
            FirstPlayerGotScore(1);
            SecondPlayerGotScore(2);
            ScoreShouldBe("Fifteen_Thirty");
        }



        private void ScoreShouldBe(string expected)
        {
            Assert.AreEqual(expected, _tennisGame.GetScore());
        }

        private void FirstPlayerGotScore(int UPPER)
        {
            for (int i = 0; i < UPPER; i++)
            {
                _tennisGame.FirstPlayerGotScore();
            }
        }

        private void SecondPlayerGotScore(int UPPER)
        {
            for (int i = 0; i < UPPER; i++)
            {
                _tennisGame.SecondPlayerGotScore();
            }
        }
    }
}